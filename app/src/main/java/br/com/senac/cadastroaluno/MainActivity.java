package br.com.senac.cadastroaluno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final int NOVO = 1 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






    }

     public boolean onContextItemSelected(MenuItem item){
        return  super.onContextItemSelected(item);
     }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.novo:
                Intent intent = new Intent(MainActivity.this,CadastroActivity.class);
                Log.i("#MENU", "novo ");
                break;

            case R.id.sobre:
                Log.i("#MENU", "sobre ");
                break;

        }
        return true;
    }



    public void novo(MenuItem item) {

        Intent intent = new Intent(MainActivity.this, CadastroActivity.class);

        startActivityForResult(intent , NOVO);

    }

    public void sobre(MenuItem item) {

        System.out.println("teste");



    }



}

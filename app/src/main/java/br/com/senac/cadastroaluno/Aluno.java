package br.com.senac.cadastroaluno;

/**
 * Created by sala304b on 08/03/2018.
 */

public class Aluno {

        private String nome;
        private String celular;
        private String endereco;
        private String email;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String telefone) {
        this.endereco = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
